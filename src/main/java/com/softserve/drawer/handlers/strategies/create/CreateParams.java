package com.softserve.drawer.handlers.strategies.create;

import lombok.Data;

@Data
public class CreateParams {
    private int rowsNum;
    private int colNum;
}

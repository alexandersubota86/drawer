package com.softserve.drawer.handlers.strategies.edit.draw;

import com.softserve.drawer.common.CanvasWrapper;
import com.softserve.drawer.handlers.strategies.ValidationException;
import com.softserve.drawer.handlers.strategies.edit.BaseParamsParser;
import org.springframework.stereotype.Component;

@Component
public class DrawParamsParser extends BaseParamsParser<DrawParams> {

    @Override
    public DrawParams parseAndValidate(String strParams, CanvasWrapper wrapper) throws ValidationException {
        var parts = strParams.split(" ");
        if (parts.length != 5) throw ValidationException.wrongParamsNumber();
        var params = new DrawParams();
        var xCol = parseAndNormalizeCoordinate(parts[1], wrapper.getColNum());
        var xRow = parseAndNormalizeCoordinate(parts[2], wrapper.getRowNum());
        var yCol = parseAndNormalizeCoordinate(parts[3], wrapper.getColNum());
        var yRow = parseAndNormalizeCoordinate(parts[4], wrapper.getRowNum());
        params.setXCol(xCol);
        params.setXRow(xRow);
        params.setYCol(yCol);
        params.setYRow(yRow);
        return params;
    }
}

package com.softserve.drawer.handlers.strategies.edit;

import com.softserve.drawer.handlers.strategies.ParamsParser;
import com.softserve.drawer.handlers.strategies.ValidationException;

public abstract class BaseParamsParser<T> implements ParamsParser<T> {

    protected int parseIntParam(String strParam) throws ValidationException {
        try {
            return Integer.parseInt(strParam);
        } catch (NumberFormatException ex) {
            throw ValidationException.wrongParamType();
        }
    }

    protected char parseCharParam(String strParam) {
        return strParam.charAt(0);
    }

    protected int parseAndNormalizeCoordinate(String strInt, int canvasDimension) throws ValidationException {
        int coord = parseIntParam(strInt);
        return coord <= 0 ? 0 : Math.min(coord, canvasDimension) - 1;
    }

}

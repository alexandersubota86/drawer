package com.softserve.drawer.handlers.strategies.edit.draw;

import com.softserve.drawer.common.CanvasWrapper;
import com.softserve.drawer.common.Command;
import com.softserve.drawer.handlers.strategies.ParamsParser;
import com.softserve.drawer.handlers.strategies.edit.BaseEditHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import static com.softserve.drawer.handlers.strategies.edit.DrawUtils.drawHorizontalLine;
import static com.softserve.drawer.handlers.strategies.edit.DrawUtils.drawVerticalLine;

@Component
@RequiredArgsConstructor
public class DrawRecHandler extends BaseEditHandler<DrawParams> {

    private final ParamsParser<DrawParams> parser;

    @Override
    protected void process(DrawParams params, CanvasWrapper wrapper) {
        var canvas = wrapper.getCanvas();
        drawHorizontalLine(canvas, params.getXCol(), params.getYCol(), params.getXRow());
        drawHorizontalLine(canvas, params.getXCol(), params.getYCol(), params.getYRow());
        drawVerticalLine(canvas, params.getXRow(), params.getYRow(), params.getXCol());
        drawVerticalLine(canvas, params.getXRow(), params.getYRow(), params.getYCol());
    }

    @Override
    public Command getTargetCommand() {
        return Command.DRAW_RECTANGLE;
    }

    @Override
    protected ParamsParser<DrawParams> getParser() {
        return parser;
    }
}

package com.softserve.drawer.handlers.strategies.edit.fill;

import com.softserve.drawer.common.CanvasWrapper;
import com.softserve.drawer.handlers.strategies.ValidationException;
import com.softserve.drawer.handlers.strategies.edit.BaseParamsParser;
import org.springframework.stereotype.Component;

@Component
public class FillParamsParser extends BaseParamsParser<FillParams> {

    @Override
    public FillParams parseAndValidate(String strParams, CanvasWrapper wrapper) throws ValidationException {
        var parts = strParams.split(" ");
        if (parts.length != 4) throw ValidationException.wrongParamsNumber();
        var params = new FillParams();
        var colNum = parseAndNormalizeCoordinate(parts[1], wrapper.getColNum());
        var rowNum = parseAndNormalizeCoordinate(parts[2], wrapper.getRowNum());
        params.setCol(colNum);
        params.setRow(rowNum);
        params.setColour(parseCharParam(parts[3]));
        return params;
    }

}

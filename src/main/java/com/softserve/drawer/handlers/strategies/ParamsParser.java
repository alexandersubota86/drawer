package com.softserve.drawer.handlers.strategies;

import com.softserve.drawer.common.CanvasWrapper;

public interface ParamsParser<T> {

    T parseAndValidate(String strParams, CanvasWrapper wrapper) throws ValidationException;

}

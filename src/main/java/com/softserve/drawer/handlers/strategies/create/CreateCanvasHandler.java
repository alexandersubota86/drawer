package com.softserve.drawer.handlers.strategies.create;

import com.softserve.drawer.common.CanvasWrapper;
import com.softserve.drawer.common.Command;
import com.softserve.drawer.handlers.strategies.BaseHandler;
import com.softserve.drawer.handlers.strategies.ParamsParser;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CreateCanvasHandler extends BaseHandler<CreateParams> {

    private final ParamsParser<CreateParams> parser;

    @Override
    protected void process(CreateParams params, CanvasWrapper wrapper) {
        wrapper.initCanvas(params.getColNum(), params.getRowsNum());
    }

    @Override
    public Command getTargetCommand() {
        return Command.CREATE_CANVAS;
    }

    @Override
    protected ParamsParser<CreateParams> getParser() {
        return parser;
    }
}

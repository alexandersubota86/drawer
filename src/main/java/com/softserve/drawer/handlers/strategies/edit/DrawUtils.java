package com.softserve.drawer.handlers.strategies.edit;

public class DrawUtils {

    public static final char LINE_CHAR = 'X';

    public static void drawVerticalLine(char[][] canvas, int xRow, int yRow, int colNum) {
        int currRow = Math.min(xRow, yRow);
        int endRow = Math.max(yRow, xRow);
        for (; currRow <= endRow; currRow++) {
            canvas[currRow][colNum] = LINE_CHAR;
        }
    }

    public static void drawHorizontalLine(char[][] canvas, int xCol, int yCol, int rowNum) {
        int currCol = Math.min(xCol, yCol);
        int endCol = Math.max(yCol, xCol);
        for (; currCol <= endCol; currCol++) {
            canvas[rowNum][currCol] = LINE_CHAR;
        }
    }

}

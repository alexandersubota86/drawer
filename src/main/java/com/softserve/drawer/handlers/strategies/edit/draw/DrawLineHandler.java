package com.softserve.drawer.handlers.strategies.edit.draw;

import com.softserve.drawer.common.CanvasWrapper;
import com.softserve.drawer.common.Command;
import com.softserve.drawer.handlers.strategies.ParamsParser;
import com.softserve.drawer.handlers.strategies.edit.BaseEditHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import static com.softserve.drawer.common.ErrorMessages.POINTS_NOT_ON_THE_SAME_LINE;
import static com.softserve.drawer.handlers.strategies.edit.DrawUtils.drawHorizontalLine;
import static com.softserve.drawer.handlers.strategies.edit.DrawUtils.drawVerticalLine;

@Component
@RequiredArgsConstructor
public class DrawLineHandler extends BaseEditHandler<DrawParams> {

    private final ParamsParser<DrawParams> parser;

    @Override
    protected void process(DrawParams params, CanvasWrapper wrapper) {
        var canvas = wrapper.getCanvas();
        if (params.getXCol() == params.getYCol()) {
            drawVerticalLine(canvas, params.getXRow(), params.getYRow(), params.getXCol());
        } else if (params.getXRow() == params.getYRow()) {
            drawHorizontalLine(canvas, params.getXCol(), params.getYCol(), params.getXRow());
        } else {
            wrapper.setError(POINTS_NOT_ON_THE_SAME_LINE);
        }
    }

    @Override
    protected ParamsParser<DrawParams> getParser() {
        return parser;
    }

    @Override
    public Command getTargetCommand() {
        return Command.DRAW_LINE;
    }
}

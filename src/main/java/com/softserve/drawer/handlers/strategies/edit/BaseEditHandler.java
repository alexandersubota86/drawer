package com.softserve.drawer.handlers.strategies.edit;

import com.softserve.drawer.common.CanvasWrapper;
import com.softserve.drawer.handlers.strategies.BaseHandler;

import static com.softserve.drawer.common.ErrorMessages.EDIT_WITHOUT_CANVAS;

public abstract class BaseEditHandler<T> extends BaseHandler<T> {

    @Override
    public void handle(String strParams, CanvasWrapper canvas) {
        if (!canvas.hasCanvas()) {
            canvas.setError(EDIT_WITHOUT_CANVAS);
            return;
        }
        super.handle(strParams, canvas);
    }

}

package com.softserve.drawer.handlers.strategies.create;

import com.softserve.drawer.common.CanvasWrapper;
import com.softserve.drawer.handlers.strategies.ValidationException;
import com.softserve.drawer.handlers.strategies.edit.BaseParamsParser;
import org.springframework.stereotype.Component;

@Component
public class CreateParamsParser extends BaseParamsParser<CreateParams> {

    @Override
    public CreateParams parseAndValidate(String strParams, CanvasWrapper wrapper) throws ValidationException {
        var parts = strParams.split(" ");
        if (parts.length != 3) throw ValidationException.wrongParamsNumber();
        var params = new CreateParams();
        params.setColNum(parseAndValidateParam(parts[1]));
        params.setRowsNum(parseAndValidateParam(parts[2]));
        return params;
    }

    private int parseAndValidateParam(String strParam) throws ValidationException {
        int param = parseIntParam(strParam);
        if (param <= 0) throw ValidationException.negativeNumber();
        return param;
    }

}

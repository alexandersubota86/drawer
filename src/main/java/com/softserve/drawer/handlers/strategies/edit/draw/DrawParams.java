package com.softserve.drawer.handlers.strategies.edit.draw;

import lombok.Data;

@Data
public class DrawParams {
    private int xRow;
    private int xCol;
    private int yRow;
    private int yCol;
}

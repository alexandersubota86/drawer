package com.softserve.drawer.handlers.strategies;

import com.softserve.drawer.common.CanvasWrapper;

public abstract class BaseHandler<T> implements Handler{

    public void handle(String strParams, CanvasWrapper wrapper) {
        T params = parseParams(strParams, wrapper);
        if (!wrapper.hasError()) {
            process(params, wrapper);
        }
    }

    private T parseParams(String strParams, CanvasWrapper wrapper) {
        var parser = getParser();
        try {
            return parser.parseAndValidate(strParams, wrapper);
        } catch (ValidationException ex) {
            wrapper.setError(ex.getMessage());
            return null;
        }
    }

    protected abstract void process(T params, CanvasWrapper wrapper);

    protected abstract ParamsParser<T> getParser();

}

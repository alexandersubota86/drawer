package com.softserve.drawer.handlers.strategies.edit.fill;

import com.softserve.drawer.common.CanvasWrapper;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.LinkedList;

public class CanvasFiller {

    private LinkedList<Point> queue = new LinkedList<>();
    private char[][] canvas;
    private char fillChar;
    private char targetChar;
    private int canvasRowNum;
    private int canvasColNum;

    public CanvasFiller(CanvasWrapper wrapper) {
        canvas = wrapper.getCanvas();
        canvasRowNum = wrapper.getRowNum();
        canvasColNum = wrapper.getColNum();
    }

    public char[][] fillArea(FillParams params) {
        targetChar = canvas[params.getRow()][params.getCol()];
        fillChar = params.getColour();
        visitPoint(params.getRow(), params.getCol());
        while (!queue.isEmpty()) {
            checkNearPoints(queue.poll());
        }
        return canvas;
    }

    private void checkNearPoints(Point point) {
        int rowNum = point.getRowNum();
        int colNum = point.getColNum();
        visitPoint(rowNum - 1, colNum);
        visitPoint(rowNum + 1, colNum);
        visitPoint(rowNum, colNum - 1);
        visitPoint(rowNum, colNum + 1);
    }

    private void visitPoint(int rowNum, int colNum) {
        if (isPointToVisit(rowNum, colNum)) {
            canvas[rowNum][colNum] = fillChar;
            queue.add(new Point(rowNum, colNum));
        }
    }

    private boolean isPointToVisit(int rowNum, int colNum) {
        return rowNum >= 0 && rowNum < canvasRowNum
                && colNum >= 0 && colNum < canvasColNum
                && canvas[rowNum][colNum] == targetChar;
    }

    @Getter
    @RequiredArgsConstructor
    private static class Point {
        private final int rowNum;
        private final int colNum;
    }

}

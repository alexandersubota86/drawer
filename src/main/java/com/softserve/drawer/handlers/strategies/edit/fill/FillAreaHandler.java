package com.softserve.drawer.handlers.strategies.edit.fill;

import com.softserve.drawer.common.CanvasWrapper;
import com.softserve.drawer.common.Command;
import com.softserve.drawer.handlers.strategies.ParamsParser;
import com.softserve.drawer.handlers.strategies.edit.BaseEditHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class FillAreaHandler extends BaseEditHandler<FillParams> {

    private final ParamsParser<FillParams> parser;

    @Override
    protected void process(FillParams params, CanvasWrapper wrapper) {
        var filler = new CanvasFiller(wrapper);
        wrapper.setCanvas(filler.fillArea(params));
    }

    @Override
    protected ParamsParser<FillParams> getParser() {
        return parser;
    }

    @Override
    public Command getTargetCommand() {
        return Command.FILL_AREA;
    }

}

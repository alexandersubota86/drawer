package com.softserve.drawer.handlers.strategies;

import com.softserve.drawer.common.Command;
import com.softserve.drawer.common.CanvasWrapper;

public interface Handler {

    void handle(String strParams, CanvasWrapper wrapper);

    Command getTargetCommand();

}

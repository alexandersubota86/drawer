package com.softserve.drawer.handlers.strategies.edit.fill;

import lombok.Data;

@Data
public class FillParams {
    private int row;
    private int col;
    private char colour;
}

package com.softserve.drawer.handlers.strategies;

import static com.softserve.drawer.common.ErrorMessages.*;

public class ValidationException extends Exception{

    private ValidationException(String error) {
        super(error, null, false, false);
    }

    public static ValidationException wrongParamType() {
        return new ValidationException(WRONG_PARAMETER_TYPE);
    }

    public static ValidationException wrongParamsNumber() {
        return new ValidationException(WRONG_PARAMETER_NUMBER);
    }

    public static ValidationException negativeNumber() {
        return new ValidationException(NEGATIVE_PARAMETER);
    }

}

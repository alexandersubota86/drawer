package com.softserve.drawer.handlers;

import com.softserve.drawer.common.Command;
import com.softserve.drawer.handlers.strategies.Handler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import static java.util.stream.Collectors.toMap;

@Service
public class HandlerFactoryImpl implements HandlerFactory{

    private Map<Command, Handler> handlers;

    @Autowired
    public HandlerFactoryImpl(Set<Handler> handlers) {
        addHandlers(handlers);
    }

    @Override
    public Handler getHandler(Command cmd) {
        return handlers.get(cmd);
    }

    private void addHandlers(Set<Handler> handlers) {
        this.handlers = handlers
                .stream()
                .collect(toMap(Handler::getTargetCommand, Function.identity()));
    }

}

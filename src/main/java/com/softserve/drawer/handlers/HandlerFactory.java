package com.softserve.drawer.handlers;

import com.softserve.drawer.common.Command;
import com.softserve.drawer.handlers.strategies.Handler;

public interface HandlerFactory {

    Handler getHandler(Command cmd);

}

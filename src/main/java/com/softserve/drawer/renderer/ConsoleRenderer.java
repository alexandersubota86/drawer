package com.softserve.drawer.renderer;

import com.softserve.drawer.common.CanvasWrapper;
import org.springframework.stereotype.Service;

import java.util.stream.IntStream;

@Service
public class ConsoleRenderer implements Renderer{

    private static final char HORIZONTAL_CHAR = '-';
    private static final char VERTICAL_CHAR = '|';

    @Override
    public void render(CanvasWrapper wrapper) {
        if (wrapper.hasError()) {
            System.out.println(wrapper.getError());
            wrapper.cleanError();
        } else {
            var matrix = wrapper.getCanvas();
            int colNum = matrix[0].length;
            drawHorizontalBorder(colNum);
            for (char[] row : matrix) {
                System.out.print(VERTICAL_CHAR);
                for (char cell : row) {
                    System.out.print(cell);
                }
                System.out.print(VERTICAL_CHAR);
                System.out.println();
            }
            drawHorizontalBorder(colNum);
        }
    }

    private void drawHorizontalBorder(int width) {
        IntStream.iterate(0, i -> i).limit(width + 2).forEach(i -> System.out.print(HORIZONTAL_CHAR));
        System.out.println();
    }

}

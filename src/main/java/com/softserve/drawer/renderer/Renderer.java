package com.softserve.drawer.renderer;

import com.softserve.drawer.common.CanvasWrapper;

public interface Renderer {

    void render(CanvasWrapper canvas);

}

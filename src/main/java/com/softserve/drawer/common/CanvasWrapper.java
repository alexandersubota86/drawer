package com.softserve.drawer.common;

import lombok.Data;
import org.apache.logging.log4j.util.Strings;

@Data
public class CanvasWrapper {

    private char[][] canvas;
    private String error;
    private int rowNum;
    private int colNum;

    public void cleanError() {
        error = null;
    }

    public void initCanvas(int colNum, int rowNum) {
        this.colNum = colNum;
        this.rowNum = rowNum;
        canvas = new char[rowNum][colNum];
        for (int i = 0; i < canvas.length; i++) {
            for (int j = 0; j < canvas[0].length; j++) {
                canvas[i][j] = ' ';
            }
        }
    }

    public boolean hasCanvas() {
        return canvas != null;
    }

    public boolean hasError() {
        return Strings.isNotBlank(error);
    }

}

package com.softserve.drawer.common;

import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;

import static java.util.stream.Collectors.toMap;

public enum Command {

    CREATE_CANVAS('C'),
    DRAW_LINE('L'),
    DRAW_RECTANGLE('R'),
    FILL_AREA('F'),
    QUIT('Q');

    Command(char cmd) {
        this.cmd = cmd;
    }

    @Getter
    private final char cmd;
    private static final Map<Character, Command> lookup;

    static {
        lookup = Arrays.stream(Command.values()).collect(toMap(Command::getCmd, Function.identity()));
    }

    public static Command of(char cmdLetter) {
        return lookup.get(Character.toUpperCase(cmdLetter));
    }

}

package com.softserve.drawer.common;

public interface ErrorMessages {

    String WRONG_PARAMETER_TYPE = "Wrong parameter type.";
    String WRONG_PARAMETER_NUMBER = "Wrong parameter number.";
    String NEGATIVE_PARAMETER = "Parameter must be a positive number.";
    String POINTS_NOT_ON_THE_SAME_LINE = "Points must be on the same line.";
    String EDIT_WITHOUT_CANVAS = "Please, create a canvas before draw any figure.";
    String EMPTY_COMMAND = "Command can not be empty.";
    String UNKNOWN_COMMAND = "Unknown command.";

}

package com.softserve.drawer.controller;

import com.google.common.base.Strings;
import com.softserve.drawer.common.CanvasWrapper;
import com.softserve.drawer.common.Command;
import com.softserve.drawer.handlers.HandlerFactory;
import com.softserve.drawer.renderer.Renderer;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.util.Scanner;

import static com.softserve.drawer.common.ErrorMessages.EMPTY_COMMAND;
import static com.softserve.drawer.common.ErrorMessages.UNKNOWN_COMMAND;

@Service
@RequiredArgsConstructor
public class CommandController implements CommandLineRunner {

    private final HandlerFactory handlerFactory;
    private final Renderer renderer;

    @Override
    public void run(String... args) throws Exception {
        try (var scan = new Scanner(System.in)) {
            var wrapper = new CanvasWrapper();
            while (processCmd(scan.nextLine(), wrapper)) {
                renderer.render(wrapper);
            }
        }
    }

    private boolean processCmd(String input, CanvasWrapper wrapper) {
        if (Strings.isNullOrEmpty(input)) {
            wrapper.setError(EMPTY_COMMAND);
            return true;
        }
        var command = getCommand(input);
        if (command == null) {
            wrapper.setError(UNKNOWN_COMMAND);
        } else if (Command.QUIT.equals(command)) {
            return false;
        } else {
            var handler = handlerFactory.getHandler(command);
            handler.handle(input, wrapper);
        }
        return true;
    }

    private Command getCommand(String input) {
        return Command.of(input.charAt(0));
    }
}

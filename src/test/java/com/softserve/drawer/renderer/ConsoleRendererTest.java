package com.softserve.drawer.renderer;

import com.softserve.drawer.common.ErrorMessages;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;

import static com.softserve.drawer.handlers.strategies.common.TestUtils.getEmptyWrapper;
import static com.softserve.drawer.handlers.strategies.common.TestUtils.getFilledWrapper;
import static org.junit.jupiter.api.Assertions.*;

public class ConsoleRendererTest {

    private static final PrintStream DEFAULT_STDOUT = System.out;

    private Renderer renderer = new ConsoleRenderer();
    private final ByteArrayOutputStream outCaptor = new ByteArrayOutputStream();

    @BeforeEach
    public void initEach() {
        System.setOut(new PrintStream(outCaptor));
    }

    @AfterEach
    public void afterEach() {
        System.setOut(DEFAULT_STDOUT);
    }

    @Test
    public void shouldAddBorder() throws IOException {
        var expectedLines = new String[] {
                "---",
                "|0|",
                "---"
        };
        renderer.render(getFilledWrapper(1, '0'));
        try (var reader = new BufferedReader(new InputStreamReader(
                new ByteArrayInputStream(outCaptor.toByteArray())))) {
            for (String expectedLine : expectedLines) {
                assertEquals(expectedLine, reader.readLine());
            }
            assertNull(reader.readLine());
        }
    }

    @Test
    public void shouldPrintError() throws IOException {
        var wrapper = getEmptyWrapper();
        wrapper.setError(ErrorMessages.EDIT_WITHOUT_CANVAS);
        renderer.render(wrapper);
        try (var reader = new BufferedReader(new InputStreamReader(
                new ByteArrayInputStream(outCaptor.toByteArray())))) {
            assertEquals(ErrorMessages.EDIT_WITHOUT_CANVAS, reader.readLine());
        }
        assertFalse(wrapper.hasError());
    }

}

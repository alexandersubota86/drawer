package com.softserve.drawer.controller;

import com.softserve.drawer.common.CanvasWrapper;
import com.softserve.drawer.common.Command;
import com.softserve.drawer.handlers.HandlerFactory;
import com.softserve.drawer.handlers.strategies.Handler;
import com.softserve.drawer.renderer.Renderer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static com.softserve.drawer.common.ErrorMessages.EMPTY_COMMAND;
import static com.softserve.drawer.common.ErrorMessages.UNKNOWN_COMMAND;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CommandControllerTest {

    private static final InputStream DEFAULT_STDIN = System.in;
    private static final String QUIT_COMMAND = "\nQ";

    @InjectMocks
    private CommandController controller;

    @Mock
    private HandlerFactory handlerFactory;
    @Mock
    private Renderer renderer;
    @Mock
    private Handler handler;
    @Captor
    private ArgumentCaptor<String> commandCaptor;
    @Captor
    private ArgumentCaptor<CanvasWrapper> wrapperCaptor;

    @AfterEach
    private void afterEach() {
        System.setIn(DEFAULT_STDIN);
    }

    @Test
    public void shouldParseCreateCanvasCommand() throws Exception {
        var input = "C 5 5";
        when(handlerFactory.getHandler(ArgumentMatchers.notNull())).thenReturn(handler);
        try (var is = new ByteArrayInputStream(prepareInput(input))) {
            System.setIn(is);
            controller.run();
            verify(handlerFactory).getHandler(Command.CREATE_CANVAS);
            verify(handler).handle(commandCaptor.capture(), wrapperCaptor.capture());
            assertEquals(input, commandCaptor.getValue());
            var wrapper = wrapperCaptor.getValue();
            assertFalse(wrapper.hasError());
            assertNull(wrapper.getError());
        }
    }

    @Test
    public void shouldParseDrawRectangleCommand() throws Exception {
        var input = "R 5 5 5 5";
        when(handlerFactory.getHandler(ArgumentMatchers.notNull())).thenReturn(handler);
        try (var is = new ByteArrayInputStream(prepareInput(input))) {
            System.setIn(is);
            controller.run();
            verify(handlerFactory).getHandler(Command.DRAW_RECTANGLE);
            verify(handler).handle(commandCaptor.capture(), wrapperCaptor.capture());
            assertEquals(input, commandCaptor.getValue());
            var wrapper = wrapperCaptor.getValue();
            assertFalse(wrapper.hasError());
            assertNull(wrapper.getError());
        }
    }

    @Test
    public void shouldParseDrawLineCommand() throws Exception {
        var input = "L 5 5 5 5";
        when(handlerFactory.getHandler(ArgumentMatchers.notNull())).thenReturn(handler);
        try (var is = new ByteArrayInputStream(prepareInput(input))) {
            System.setIn(is);
            controller.run();
            verify(handlerFactory).getHandler(Command.DRAW_LINE);
            verify(handler).handle(commandCaptor.capture(), wrapperCaptor.capture());
            assertEquals(input, commandCaptor.getValue());
            var wrapper = wrapperCaptor.getValue();
            assertFalse(wrapper.hasError());
            assertNull(wrapper.getError());
        }
    }

    @Test
    public void shouldParseFillCommand() throws Exception {
        var input = "F 5 5 5";
        when(handlerFactory.getHandler(ArgumentMatchers.notNull())).thenReturn(handler);
        try (var is = new ByteArrayInputStream(prepareInput(input))) {
            System.setIn(is);
            controller.run();
            verify(handlerFactory).getHandler(Command.FILL_AREA);
            verify(handler).handle(commandCaptor.capture(), wrapperCaptor.capture());
            assertEquals(input, commandCaptor.getValue());
            var wrapper = wrapperCaptor.getValue();
            assertFalse(wrapper.hasError());
            assertNull(wrapper.getError());
        }
    }

    @Test
    public void shouldAddErrorIfInputEmpty() throws Exception {
        var input = "";
        try (var is = new ByteArrayInputStream(prepareInput(input))) {
            System.setIn(is);
            controller.run();
            verify(renderer).render(wrapperCaptor.capture());
            var wrapper = wrapperCaptor.getValue();
            assertTrue(wrapper.hasError());
            assertEquals(EMPTY_COMMAND, wrapper.getError());
        }
    }

    @Test
    public void shouldAddErrorIfInputIsSpace() throws Exception {
        var input = " ";
        try (var is = new ByteArrayInputStream(prepareInput(input))) {
            System.setIn(is);
            controller.run();
            verify(renderer).render(wrapperCaptor.capture());
            var wrapper = wrapperCaptor.getValue();
            assertTrue(wrapper.hasError());
            assertEquals(UNKNOWN_COMMAND, wrapper.getError());
        }
    }

    @Test
    public void shouldAddErrorIfUnknownCommand() throws Exception {
        var input = "W";
        try (var is = new ByteArrayInputStream(prepareInput(input))) {
            System.setIn(is);
            controller.run();
            verify(renderer).render(wrapperCaptor.capture());
            var wrapper = wrapperCaptor.getValue();
            assertTrue(wrapper.hasError());
            assertEquals(UNKNOWN_COMMAND, wrapper.getError());
        }
    }

    private static byte[] prepareInput(String command) {
        return (command + QUIT_COMMAND).getBytes();
    }

}

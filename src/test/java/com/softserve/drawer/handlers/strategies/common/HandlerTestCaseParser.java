package com.softserve.drawer.handlers.strategies.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class HandlerTestCaseParser {

    private static final String START_TEST_CASE = "----START----";
    private static final String END_TEST_CASES = "-----END-----";
    private static final String CANVAS_SIZE_BLOCK = "CANVAS_SIZE";
    private static final String INPUT_CANVAS = "INPUT_CANVAS";
    private static final String INPUT_BLOCK = "INPUT";
    private static final String OUTPUT_BLOCK = "OUTPUT";
    private static final char EMPTY_CELL_SIGN = '0';

    private static final String FOLDER = "src/test/resources/test_cases/";

    public static List<HandlerTestCase> readTestCases(String testCaseFile) throws FileNotFoundException {
        var testCases = new ArrayList<HandlerTestCase>();
        try (var scan = new Scanner(new File(FOLDER + testCaseFile))) {
            while (scan.hasNext()) {
                if (START_TEST_CASE.equals(scan.nextLine())) {
                    var testCase = parseTestCase(scan);
                    testCases.add(testCase);
                }
            }
        }
        return testCases;
    }

    private static HandlerTestCase parseTestCase(Scanner scan) {
        var testCase = new HandlerTestCase();
        String line = scan.nextLine();
        testCase.setNumber(Integer.parseInt(line));
        loop : while (scan.hasNext()) {
            line = scan.nextLine();
            switch (line) {
                case CANVAS_SIZE_BLOCK:
                    setCanvasSize(scan, testCase);
                    continue;
                case INPUT_BLOCK:
                    testCase.setCmdParams(scan.nextLine());
                    continue;
                case OUTPUT_BLOCK:
                    testCase.setResultCanvas(parseCanvas(scan, testCase.getRowNum(), testCase.getColNum()));
                    continue;
                case INPUT_CANVAS:
                    testCase.setInitCanvas(parseCanvas(scan, testCase.getRowNum(), testCase.getColNum()));
                    continue;
                case END_TEST_CASES:
                    break loop;
            }
        }
        initDefaultCanvas(testCase);
        return testCase;
    }

    private static void setCanvasSize(Scanner scan, HandlerTestCase testCase) {
        String[] parts = scan.nextLine().split(" ");
        testCase.setRowNum(Integer.parseInt(parts[0]));
        testCase.setColNum(Integer.parseInt(parts[1]));
    }

    private static void initDefaultCanvas(HandlerTestCase testCase) {
        var canvas = testCase.getInitCanvas();
        if (canvas == null) {
            canvas = new char[testCase.getRowNum()][testCase.getColNum()];
            for (int i = 0; i < canvas.length; i++) {
                for (int j = 0; j < canvas[0].length; j++) {
                    canvas[i][j] = EMPTY_CELL_SIGN;
                }
            }
            testCase.setInitCanvas(canvas);
        }
    }

    private static char[][] parseCanvas(Scanner scan, int rowNum, int colNum) {
        var canvas = new char[rowNum][colNum];
        for (int i = 0; i < rowNum; i++) {
            var line = scan.nextLine();
            canvas[i] = line.toCharArray();
        }
        return canvas;
    }

}

package com.softserve.drawer.handlers.strategies.edit.fill;

import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static com.softserve.drawer.handlers.strategies.common.TestUtils.getWrapperWithCanvas;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CanvasFillerTest {

    private static final char LINE = 'X';
    private static final char AREA = ' ';
    private static final char FILL = '5';

    @Test
    public void shouldFillSquareArea() {
        var canvas = new char[][] {
            {AREA, AREA, AREA},
            {LINE, LINE, LINE},
            {AREA, AREA, AREA}
        };
        var wrapper = getWrapperWithCanvas(canvas);
        var result = new char[][]{
            {FILL, FILL, FILL},
            {LINE, LINE, LINE},
            {AREA, AREA, AREA}
        };
        var filler = new CanvasFiller(wrapper);
        var params = new FillParams();
        params.setCol(0);
        params.setRow(0);
        params.setColour(FILL);
        canvas = filler.fillArea(params);
        assertTrue(Arrays.deepEquals(result, canvas));
    }

    @Test
    public void shouldFillSnakeArea() {
        var canvas = new char[][] {
            {AREA, LINE, AREA, AREA, AREA},
            {AREA, LINE, AREA, LINE, AREA},
            {AREA, LINE, AREA, LINE, AREA},
            {AREA, LINE, AREA, LINE, AREA},
            {AREA, AREA, AREA, LINE, AREA}
        };
        var wrapper = getWrapperWithCanvas(canvas);
        var result = new char[][]{
            {FILL, LINE, FILL, FILL, FILL},
            {FILL, LINE, FILL, LINE, FILL},
            {FILL, LINE, FILL, LINE, FILL},
            {FILL, LINE, FILL, LINE, FILL},
            {FILL, FILL, FILL, LINE, FILL}
        };
        var filler = new CanvasFiller(wrapper);
        var params = new FillParams();
        params.setCol(0);
        params.setRow(0);
        params.setColour(FILL);
        canvas = filler.fillArea(params);
        assertTrue(Arrays.deepEquals(result, canvas));
    }

    @Test
    public void shouldFillLine() {
        var canvas = new char[][] {
            {LINE, LINE, LINE, LINE, LINE},
            {LINE, AREA, AREA, AREA, LINE},
            {LINE, AREA, LINE, AREA, LINE},
            {LINE, AREA, AREA, AREA, LINE},
            {LINE, LINE, LINE, LINE, LINE}
        };
        var wrapper = getWrapperWithCanvas(canvas);
        var result = new char[][]{
            {FILL, FILL, FILL, FILL, FILL},
            {FILL, AREA, AREA, AREA, FILL},
            {FILL, AREA, LINE, AREA, FILL},
            {FILL, AREA, AREA, AREA, FILL},
            {FILL, FILL, FILL, FILL, FILL}
        };
        var filler = new CanvasFiller(wrapper);
        var params = new FillParams();
        params.setCol(4);
        params.setRow(4);
        params.setColour(FILL);
        canvas = filler.fillArea(params);
        assertTrue(Arrays.deepEquals(result, canvas));
    }

    @Test
    public void shouldNotFillAreaIfPointOnLine() {
        var canvas = new char[][] {
            {LINE, LINE, LINE, LINE, LINE},
            {LINE, AREA, AREA, AREA, LINE},
            {LINE, AREA, LINE, AREA, LINE},
            {LINE, AREA, AREA, AREA, LINE},
            {LINE, LINE, LINE, LINE, LINE}
        };
        var wrapper = getWrapperWithCanvas(canvas);
        var result = new char[][]{
            {LINE, LINE, LINE, LINE, LINE},
            {LINE, AREA, AREA, AREA, LINE},
            {LINE, AREA, FILL, AREA, LINE},
            {LINE, AREA, AREA, AREA, LINE},
            {LINE, LINE, LINE, LINE, LINE}
        };
        var filler = new CanvasFiller(wrapper);
        var params = new FillParams();
        params.setCol(2);
        params.setRow(2);
        params.setColour(FILL);
        canvas = filler.fillArea(params);
        assertTrue(Arrays.deepEquals(result, canvas));
    }

}

package com.softserve.drawer.handlers.strategies.edit.fill;

import com.softserve.drawer.handlers.strategies.ValidationException;
import org.junit.jupiter.api.Test;

import static com.softserve.drawer.common.ErrorMessages.WRONG_PARAMETER_NUMBER;
import static com.softserve.drawer.common.ErrorMessages.WRONG_PARAMETER_TYPE;
import static com.softserve.drawer.handlers.strategies.common.TestUtils.getEmptyWrapper;
import static com.softserve.drawer.handlers.strategies.common.TestUtils.getWrapperWithCanvas;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FillParamsParserTest {

    private FillParamsParser parser = new FillParamsParser();

    @Test
    public void shouldParseParams() throws ValidationException {
        var input = "F 1 2 e";
        var result = parser.parseAndValidate(input, getWrapperWithCanvas(5));
        assertEquals(0, result.getCol());
        assertEquals(1, result.getRow());
        assertEquals('e', result.getColour());
    }

    @Test
    public void shouldThrowIfWrongCoordinateType() {
        var input = "F C 5 e";
        Throwable ex = assertThrows(ValidationException.class, () -> {
            parser.parseAndValidate(input, getWrapperWithCanvas(5));
        });
        assertEquals(WRONG_PARAMETER_TYPE, ex.getMessage());
    }

    @Test
    public void shouldNormalizeNegativeParams() throws ValidationException {
        var input = "F -1 -1 e";
        var result = parser.parseAndValidate(input, getWrapperWithCanvas(5));
        assertEquals(0, result.getCol());
        assertEquals(0, result.getRow());
    }

    @Test
    public void shouldNormalizeBiggerParams() throws ValidationException {
        var input = "F 6 6 e";
        var result = parser.parseAndValidate(input, getWrapperWithCanvas(5));
        assertEquals(4, result.getCol());
        assertEquals(4, result.getRow());
    }

    @Test
    public void shouldThrowIfWrongLessParameters() {
        var input = "F 5";
        Throwable ex = assertThrows(ValidationException.class, () -> {
            parser.parseAndValidate(input, getEmptyWrapper());
        });
        assertEquals(WRONG_PARAMETER_NUMBER, ex.getMessage());
    }

    @Test
    public void shouldThrowIfMoreParameters() {
        var input = "F 5 5 e 5 5";
        Throwable ex = assertThrows(ValidationException.class, () -> {
            parser.parseAndValidate(input, getEmptyWrapper());
        });
        assertEquals(WRONG_PARAMETER_NUMBER, ex.getMessage());
    }

}

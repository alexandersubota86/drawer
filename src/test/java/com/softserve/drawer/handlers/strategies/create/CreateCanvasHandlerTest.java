package com.softserve.drawer.handlers.strategies.create;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.softserve.drawer.common.ErrorMessages.NEGATIVE_PARAMETER;
import static com.softserve.drawer.handlers.strategies.common.TestUtils.getEmptyWrapper;
import static com.softserve.drawer.handlers.strategies.common.TestUtils.getWrapperWithCanvas;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class CreateCanvasHandlerTest {

    @InjectMocks
    private CreateCanvasHandler handler;
    @Spy
    private CreateParamsParser parser;

    @Test
    public void shouldAddNewCanvas() {
        var wrapper = getEmptyWrapper();
        handler.handle("C 1 5", wrapper);
        assertFalse(wrapper.hasError());
        assertNotNull(wrapper.getCanvas());
        assertEquals(wrapper.getColNum(), 1);
        assertEquals(wrapper.getRowNum(), 5);
        var canvas = wrapper.getCanvas();
        assertEquals(canvas.length, 5);
        assertEquals(canvas[0].length, 1);
    }

    @Test
    public void shouldAddErrorIfNegativeColumnNumber() {
        var wrapper = getEmptyWrapper();
        handler.handle("C -1 5", wrapper);
        assertTrue(wrapper.hasError());
        assertEquals(NEGATIVE_PARAMETER, wrapper.getError());
    }

    @Test
    public void shouldAddErrorIfNegativeRowNumber() {
        var wrapper = getEmptyWrapper();
        handler.handle("C 1 -5", wrapper);
        assertTrue(wrapper.hasError());
        assertEquals(NEGATIVE_PARAMETER, wrapper.getError());
    }

    @Test
    public void shouldReplaceExistingCanvas() {
        var wrapper = getWrapperWithCanvas(1);
        handler.handle("C 9 5", wrapper);
        assertFalse(wrapper.hasError());
        assertEquals(wrapper.getRowNum(), 5);
        assertEquals(wrapper.getColNum(), 9);
        var canvas = wrapper.getCanvas();
        assertEquals(canvas.length, 5);
        assertEquals(canvas[0].length, 9);
    }

}

package com.softserve.drawer.handlers.strategies.common;

import com.softserve.drawer.common.CanvasWrapper;

public class TestUtils {

    public static CanvasWrapper getEmptyWrapper() {
        return new CanvasWrapper();
    }

    public static CanvasWrapper getWrapperWithCanvas() {
        return getWrapperWithCanvas(1);
    }

    public static CanvasWrapper getWrapperWithCanvas(int dimension) {
        return getWrapperWithCanvas(new char[dimension][dimension]);
    }

    public static CanvasWrapper getFilledWrapper(int dimension, char fillChar) {
        return getWrapperWithCanvas(getFilledCanvas(dimension, fillChar));
    }

    public static CanvasWrapper getWrapperWithCanvas(char[][] canvas) {
        var wrapper = new CanvasWrapper();
        wrapper.setRowNum(canvas.length);
        wrapper.setColNum(canvas[0].length);
        wrapper.setCanvas(canvas);
        return wrapper;
    }

    public static char[][] getFilledCanvas(int dimension, char fillChar) {
        var canvas = new char[dimension][dimension];
        for (var row : canvas) {
            for (int i = 0; i < dimension; i++) {
                row[i] = fillChar;
            }
        }
        return canvas;
    }

}

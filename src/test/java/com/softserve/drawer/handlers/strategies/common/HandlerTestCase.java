package com.softserve.drawer.handlers.strategies.common;

import lombok.Data;

@Data
public class HandlerTestCase {
    private int number;
    private int rowNum;
    private int colNum;
    private char[][] initCanvas;
    private char[][] resultCanvas;
    private String cmdParams;
}

package com.softserve.drawer.handlers.strategies.edit;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static com.softserve.drawer.handlers.strategies.common.TestUtils.getFilledCanvas;
import static com.softserve.drawer.handlers.strategies.edit.DrawUtils.LINE_CHAR;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DrawUtilsTest {

    @Test
    public void shouldDrawHorizontalLine() {
        var canvas = getFilledCanvas(5, ' ');
        var resultCanvas = getFilledCanvas(5, ' ');
        for (int i = 0; i < 5; i++) {
            resultCanvas[3][i] = LINE_CHAR;
        }
        DrawUtils.drawHorizontalLine(canvas, 0, 4, 3);
        assertTrue(Arrays.deepEquals(resultCanvas, canvas));
    }

    @Test
    public void shouldDrawHorizontalLineWithUnorderedParams() {
        var canvas = getFilledCanvas(5, ' ');
        var resultCanvas = getFilledCanvas(5, ' ');
        for (int i = 0; i < 5; i++) {
            resultCanvas[3][i] = LINE_CHAR;
        }
        DrawUtils.drawHorizontalLine(canvas, 4, 0, 3);
        assertTrue(Arrays.deepEquals(resultCanvas, canvas));
    }

    @Test
    public void shouldDrawVerticalLine() {
        var canvas = getFilledCanvas(5, ' ');
        var resultCanvas = getFilledCanvas(5, ' ');
        for (int i = 0; i < 5; i++) {
            resultCanvas[i][3] = LINE_CHAR;
        }
        DrawUtils.drawVerticalLine(canvas, 0, 4, 3);
        assertTrue(Arrays.deepEquals(resultCanvas, canvas));
    }

    @Test
    public void shouldDrawVerticalLineWithUnorderedParams() {
        var canvas = getFilledCanvas(5, ' ');
        var resultCanvas = getFilledCanvas(5, ' ');
        for (int i = 0; i < 5; i++) {
            resultCanvas[i][3] = LINE_CHAR;
        }
        DrawUtils.drawVerticalLine(canvas, 4, 0, 3);
        assertTrue(Arrays.deepEquals(resultCanvas, canvas));
    }



}

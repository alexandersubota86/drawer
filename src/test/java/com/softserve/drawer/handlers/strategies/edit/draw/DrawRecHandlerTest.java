package com.softserve.drawer.handlers.strategies.edit.draw;

import com.softserve.drawer.common.CanvasWrapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.FileNotFoundException;
import java.util.Arrays;

import static com.softserve.drawer.common.ErrorMessages.*;
import static com.softserve.drawer.common.ErrorMessages.EDIT_WITHOUT_CANVAS;
import static com.softserve.drawer.handlers.strategies.common.HandlerTestCaseParser.readTestCases;
import static com.softserve.drawer.handlers.strategies.common.TestUtils.getEmptyWrapper;
import static com.softserve.drawer.handlers.strategies.common.TestUtils.getWrapperWithCanvas;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class DrawRecHandlerTest {

    private static final String TEST_CASES_FILE = "draw_rectangle_test_cases.txt";

    @InjectMocks
    private DrawRecHandler handler;
    @Spy
    private DrawParamsParser parser;

    @Test
    public void shouldDrawRectangle() throws FileNotFoundException {
        var testCases = readTestCases(TEST_CASES_FILE);
        for (var testCase : testCases) {
            var canvasWrapper = new CanvasWrapper();
            canvasWrapper.setCanvas(testCase.getInitCanvas());
            canvasWrapper.setRowNum(testCase.getRowNum());
            canvasWrapper.setColNum(testCase.getColNum());
            handler.handle(testCase.getCmdParams(), canvasWrapper);
            assertFalse(canvasWrapper.hasError());
            assertTrue(Arrays.deepEquals(testCase.getResultCanvas(), canvasWrapper.getCanvas()),
                    "Test case " + testCase.getNumber() + " failed");
        }
    }

    @Test
    public void shouldAddErrorIfWrongParamType() {
        var wrapper = getWrapperWithCanvas();
        handler.handle("R 5 5 L 5", wrapper);
        assertTrue(wrapper.hasError());
        assertEquals(WRONG_PARAMETER_TYPE, wrapper.getError());
    }

    @Test
    public void shouldAddErrorIfParamsNotSpecified() {
        var wrapper = getWrapperWithCanvas();
        handler.handle("R ", wrapper);
        assertTrue(wrapper.hasError());
        assertEquals(WRONG_PARAMETER_NUMBER, wrapper.getError());
    }

    @Test
    public void shouldAddErrorIfWrongParamNumber() {
        var wrapper = getWrapperWithCanvas();
        handler.handle("R 5 5 5 5 5", wrapper);
        assertTrue(wrapper.hasError());
        assertEquals(WRONG_PARAMETER_NUMBER, wrapper.getError());
    }

    @Test
    public void shouldAddErrorIfEditWithoutCanvas() {
        var wrapper = getEmptyWrapper();
        handler.handle("R 5 5 3 3 ", wrapper);
        assertTrue(wrapper.hasError());
        assertEquals(EDIT_WITHOUT_CANVAS, wrapper.getError());
    }
}

package com.softserve.drawer.handlers.strategies.edit.draw;

import com.softserve.drawer.handlers.strategies.ValidationException;
import org.junit.jupiter.api.Test;

import static com.softserve.drawer.common.ErrorMessages.WRONG_PARAMETER_NUMBER;
import static com.softserve.drawer.common.ErrorMessages.WRONG_PARAMETER_TYPE;
import static com.softserve.drawer.handlers.strategies.common.TestUtils.getEmptyWrapper;
import static com.softserve.drawer.handlers.strategies.common.TestUtils.getWrapperWithCanvas;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DrawParamsParserTest {

    private DrawParamsParser parser = new DrawParamsParser();

    @Test
    public void shouldParseParams() throws ValidationException {
        var input = "L 1 2 3 4";
        var result = parser.parseAndValidate(input, getWrapperWithCanvas(5));
        assertEquals(0, result.getXCol());
        assertEquals(1, result.getXRow());
        assertEquals(2, result.getYCol());
        assertEquals(3, result.getYRow());
    }

    @Test
    public void shouldThrowIfWrongParameterType() {
        var input = "L C 5 5 5";
        Throwable ex = assertThrows(ValidationException.class, () -> {
            parser.parseAndValidate(input, getWrapperWithCanvas(5));
        });
        assertEquals(WRONG_PARAMETER_TYPE, ex.getMessage());
    }

    @Test
    public void shouldNormalizeNegativeParams() throws ValidationException {
        var input = "L -1 -1 -1 -1";
        var result = parser.parseAndValidate(input, getWrapperWithCanvas(5));
        assertEquals(0, result.getXCol());
        assertEquals(0, result.getXRow());
        assertEquals(0, result.getYCol());
        assertEquals(0, result.getYRow());
    }

    @Test
    public void shouldNormalizeBiggerParams() throws ValidationException {
        var input = "L 6 6 6 6";
        var result = parser.parseAndValidate(input, getWrapperWithCanvas(5));
        assertEquals(4, result.getXCol());
        assertEquals(4, result.getXRow());
        assertEquals(4, result.getYCol());
        assertEquals(4, result.getYRow());
    }

    @Test
    public void shouldThrowIfWrongLessParameters() {
        var input = "L 5";
        Throwable ex = assertThrows(ValidationException.class, () -> {
            parser.parseAndValidate(input, getEmptyWrapper());
        });
        assertEquals(WRONG_PARAMETER_NUMBER, ex.getMessage());
    }

    @Test
    public void shouldThrowIfMoreParameters() {
        var input = "L 5 5 5 5 5";
        Throwable ex = assertThrows(ValidationException.class, () -> {
            parser.parseAndValidate(input, getEmptyWrapper());
        });
        assertEquals(WRONG_PARAMETER_NUMBER, ex.getMessage());
    }

}

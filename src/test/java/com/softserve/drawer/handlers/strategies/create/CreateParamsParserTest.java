package com.softserve.drawer.handlers.strategies.create;

import com.softserve.drawer.handlers.strategies.ValidationException;
import org.junit.jupiter.api.Test;

import static com.softserve.drawer.common.ErrorMessages.*;
import static com.softserve.drawer.handlers.strategies.common.TestUtils.getEmptyWrapper;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CreateParamsParserTest {

    private CreateParamsParser parser = new CreateParamsParser();

    @Test
    public void shouldParseParams() throws ValidationException {
        var input = "C 5 9";
        var result = parser.parseAndValidate(input, getEmptyWrapper());
        assertEquals(5, result.getColNum());
        assertEquals(9, result.getRowsNum());
    }

    @Test
    public void shouldThrowIfWrongFirstParameterType() {
        var input = "C C 5";
        Throwable ex = assertThrows(ValidationException.class, () -> {
            parser.parseAndValidate(input, getEmptyWrapper());
        });
        assertEquals(WRONG_PARAMETER_TYPE, ex.getMessage());
    }

    @Test
    public void shouldThrowIfWrongSecondParameterType() {
        var input = "C 5 C";
        Throwable ex = assertThrows(ValidationException.class, () -> {
            parser.parseAndValidate(input, getEmptyWrapper());
        });
        assertEquals(WRONG_PARAMETER_TYPE, ex.getMessage());
    }

    @Test
    public void shouldThrowIfWrongLessParameters() {
        var input = "C 5";
        Throwable ex = assertThrows(ValidationException.class, () -> {
            parser.parseAndValidate(input, getEmptyWrapper());
        });
        assertEquals(WRONG_PARAMETER_NUMBER, ex.getMessage());
    }

    @Test
    public void shouldThrowIfMoreParameters() {
        var input = "C 5 5 5";
        Throwable ex = assertThrows(ValidationException.class, () -> {
            parser.parseAndValidate(input, getEmptyWrapper());
        });
        assertEquals(WRONG_PARAMETER_NUMBER, ex.getMessage());
    }

    @Test
    public void shouldThrowIfNegativeFirstParameter() {
        var input = "C -5 5";
        Throwable ex = assertThrows(ValidationException.class, () -> {
            parser.parseAndValidate(input, getEmptyWrapper());
        });
        assertEquals(NEGATIVE_PARAMETER, ex.getMessage());
    }

    @Test
    public void shouldThrowIfNegativeSecondParameter() {
        var input = "C 5 -5";
        Throwable ex = assertThrows(ValidationException.class, () -> {
            parser.parseAndValidate(input, getEmptyWrapper());
        });
        assertEquals(NEGATIVE_PARAMETER, ex.getMessage());
    }

}

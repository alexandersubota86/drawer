###Build and Run

The application requires java 11 to be installed on your computer.

To build the project execute following command:

    for Linux: ./gradlew clean build
    for Windows .\gradlew clean build

To run the application execute following command:

    for Linux: java -jar ./build/libs/drawer-0.0.1-SNAPSHOT.jar
    for Windows: java -jar .\build\libs\drawer-0.0.1-SNAPSHOT.jar

###Commands:
The first char of the instruction describes the command.
Each command might have some -- space separated -- parameters

* Create a new canvas (w: width, h: height): `C w h`
* Draw a line (only vertical and horizontal lines are supported): `L x1 y1 x2 y2`
* Draw a rectangle: `R x1 y1 x2 y2`
* Fill the connected area with c colour from x, y coordinates: `F x y c`
* Quit: `Q`

Points outside to the canvas are transferred to the closest edge points.




